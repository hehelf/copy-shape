﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TestScript : MonoBehaviour
{
    public Vector3[] pos = new Vector3[3];
    public GameObject Wall;
    public SticmanIK StIK;
    void Start()
    {
        
    }

    public void ToggleIK(bool active){
        StIK.enabledIK= active;
    }

    public void MoveWall(int posind){
        Wall.transform.DOMove(pos[posind],1);
    }
}
