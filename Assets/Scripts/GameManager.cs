﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;
using GameAnalyticsSDK;

public class GameManager : MonoBehaviour
{
    
    public Camera MenuCamera;
    public Image BlackScreen;
    public GameObject man;
    public Transform[] CamPos = new Transform[3];
    public List<GameObject> CtrlButtons = new List<GameObject>();
    public TextMeshProUGUI lvlText;
    public static int lvl;

    SticmanIK SIK;
    SceneControl SC;
    Camera MainCamera;
    WallControl WC;

    void Awake(){
        GameAnalytics.Initialize();
    }
    void Start()
    {
        SIK = man.GetComponentInChildren<SticmanIK>();
        SC= FindObjectOfType<SceneControl>();
        WC = GetComponent<WallControl>();
        MainCamera =  Camera.main;
        MenuCamera.depth = 1;
        MainCamera.depth = -1;
        Load();
        UpdText();
    }

    
    public void MoveCamera(int pos){
        var cam = Camera.main;
        float dur= 0.5f;
        cam.transform.DOMove(CamPos[pos].position,dur);
        cam.transform.DORotateQuaternion(CamPos[pos].rotation,dur*2);

        if(pos==0) StartCoroutine(SetOrtoCam(dur*2.05f));
        else cam.orthographic = false;
        
    }

    public IEnumerator SetOrtoCam(float sec){
        yield return new WaitForSeconds(sec);
        Camera.main.orthographic=true;
        CtrlButtons[0].SetActive(true);
        
    }

    public IEnumerator Win(){
        lvl++;
        WC.UpdWallInd();
        Save();
        SIK.ToggleRagdoll(false);
        SIK.FinSetStartPos(0.5f);
        SIK.GoOnFloor(0.5f);
        SIK.animator.SetTrigger("Dance");
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, $"Complete level {lvl}");
        yield return new WaitForSeconds(0.3f);
        SC.Clap();
        // yield return new WaitForSeconds(dur);
        SIK.enabledIK = false;

        
    }
   
    
    public void ShowButtons(){
        if(SticmanIK.Fail){
            CtrlButtons[1].SetActive(true);
        }else{
            CtrlButtons[2].SetActive(true);
        }
    }

    public void StartGame(){
        BlackScreen.DOColor(new Color(0,0,0,0),1f);
        Invoke("HideBlack",1f);
        MenuCamera.depth = -1;
        MenuCamera.gameObject.SetActive(false);
        MainCamera.depth = 1;
        CtrlButtons[3].SetActive(false);
        CtrlButtons[0].SetActive(true);
        SIK.GetComponentInChildren<Renderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;// опять костыль из-за дерканных контроллеров *см. ControllerScript.ToggleActive()
        GetComponent<WallControl>().enabled = true;
    }

     public void Restart(bool Next){
        WC.RestartWall(Next);
        SIK.ToggleControllers(true);
        MoveCamera(0);
        

        if(!SticmanIK.Fail){
            SIK.animator.SetBool("Exit",true);
            // SIK.animator.StopPlayback();
            // SIK.animator.Update(0);
            // SIK.SetStartPos();
            StartCoroutine(SIK.SetStartPos());
        }

        SticmanIK.Fail=false;
        CtrlButtons[1].SetActive(false);
        CtrlButtons[2].SetActive(false);
        SIK.enabledIK = true;
        UpdText();
     }

    void HideBlack(){
        BlackScreen.gameObject.SetActive(false);
    }

    void Save(){
        PlayerPrefs.SetInt("lvl",lvl);
        PlayerPrefs.SetInt("wallInd",WC.WallIndex);
        PlayerPrefs.Save();
    }

    void Load(){
        lvl = PlayerPrefs.GetInt("lvl",1);
        WC.WallIndex = PlayerPrefs.GetInt("wallInd",0);
    }
    void UpdText(){
        lvlText.text = "level "+lvl;
    }
}
