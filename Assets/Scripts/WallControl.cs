﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;


public class WallControl : MonoBehaviour
{
    public List<GameObject> Walls = new List<GameObject>();
    public float WallSpawnDist;
     public float WallFinPos;
    public float WallDuration;
    public int WallIndex;
    public GameObject CurWallGO;
    public bool isMove=false;
    public bool CheckPoint=false;
    SticmanIK SIK;
    SceneControl SC;
    GameManager GM;


    void Start()
    {
        SIK = FindObjectOfType<SticmanIK>();
        GM = GetComponent<GameManager>();
        SpawnWall();

        // Fail=false;
    }

    void Update(){

        if(CurWallGO == null){
            SpawnWall();
        }
    }

    void FixedUpdate(){
        if(isMove){
            Move();
            
            if(CurWallGO.transform.position.z>2&&!CheckPoint){ 
                 CheckPoint = true;
                 if(!SticmanIK.Fail){
                    StartCoroutine(GM.Win());
                    GM.MoveCamera(2);
                 }   
                 Debug.Log("Show");
                
            }
            if(CurWallGO.transform.position.z>WallFinPos){ 
                 isMove=false;
                 GM.ShowButtons();
            }
        }

    }
    void SpawnWall(){
        int i = WallIndex;
        CurWallGO = Instantiate(Walls[i],Vector3.back*WallSpawnDist,Quaternion.Euler(0,-90,0));
        CurWallGO.GetComponent<Wall>().SIK=SIK;

    }

    public void StartMovement(){

        SIK.animator.enabled = true;
        GM.CtrlButtons[0].SetActive(false);
        isMove=true;  
        SIK.SaveCurrentPose();  
        SIK.ToggleControllers(false); 
        GM.MoveCamera(1);
        StopAllCoroutines();
        SIK.animator.SetBool("Exit",false);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, $"Start level {GameManager.lvl}");     
          
    }
    public void Move(){
        CurWallGO.transform.position= CurWallGO.transform.position + Vector3.forward * Time.fixedDeltaTime*WallDuration;
    }

    public void RestartWall(bool Next){
        // if (Next){
        //     if(WallIndex<Walls.Count-1) WallIndex++;
        //     else WallIndex=0;
        // }
      
        Destroy(CurWallGO);
        
        isMove=false;
        CheckPoint = false;
        
    }
    
    public void UpdWallInd(){
        if(WallIndex<Walls.Count-1) WallIndex++;
        else WallIndex=0;
    }

}
