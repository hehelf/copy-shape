﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Lean.Touch;
using DG.Tweening;
// [ExecuteInEditMode]
public class SticmanIK : MonoBehaviour {

	public float PushForce;
	public float PushVelocity;

	public bool enabledIK = true;
	public GameObject fullController;
	public GameObject leftFootIKGoal = null;
	public GameObject rightFootIKGoal = null;
	public GameObject leftHandIKGoal = null;
	public GameObject rightHandIKGoal = null;

	
	public GameObject leftKneeIKHint = null;
	public GameObject rightKneeIKHint = null;
	public GameObject leftElbowIKHint = null;
	public GameObject rightElbowIKHint = null;
	public float GoalWeight = 0;
	public float hintsWeight = 0;
	public Animator animator;

	public static bool Fail=false;
	LeanSelect LS;
	BodyMember RightHand;
	BodyMember LeftHand;
	BodyMember RightLeg;
	BodyMember LeftLeg;

	List<Rigidbody> Ragdol = new List<Rigidbody>();

	Vector3 ManStartPos = new Vector3(0,-0.25f,0);
	Vector3[] CurrentPose = new Vector3[10];

	Vector3 StartPelvis = new Vector3(-0.001752943f,-0.1942481f,-0.03774764f);
	Transform pelvis;


	protected GameObject CreateIKController(string name)
	{
		GameObject obj =  GameObject.CreatePrimitive(PrimitiveType.Sphere);
		obj.name = name;
		obj.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
		obj.transform.parent = transform.parent;
		return obj;
	}
	Transform BoneTrasform(int i){
		return animator.GetBoneTransform((HumanBodyBones)i);
	}


	void Start () {
		animator = GetComponent<Animator>();
		LS = FindObjectOfType<LeanSelect>();

	
		if (leftFootIKGoal == null)  leftFootIKGoal = CreateIKController("LF");
		if (rightFootIKGoal == null) rightFootIKGoal = CreateIKController("RF");
		if (leftHandIKGoal == null) leftHandIKGoal = CreateIKController("LH");
		if (rightHandIKGoal == null) rightHandIKGoal = CreateIKController("RH");

		if (leftKneeIKHint == null)  leftKneeIKHint = CreateIKController("LK");
		if (rightKneeIKHint == null) rightKneeIKHint = CreateIKController("RK");
		if (leftElbowIKHint == null) leftElbowIKHint = CreateIKController("LE");
		if (rightElbowIKHint == null) rightElbowIKHint = CreateIKController("RE");

		RightHand = new BodyMember(animator,rightHandIKGoal,rightElbowIKHint,HumanBodyBones.RightUpperArm,HumanBodyBones.RightLowerArm,HumanBodyBones.RightHand);
		LeftHand = new BodyMember(animator,leftHandIKGoal,leftElbowIKHint,HumanBodyBones.LeftUpperArm,HumanBodyBones.LeftLowerArm,HumanBodyBones.LeftHand);
		RightLeg = new BodyMember(animator,rightFootIKGoal,rightKneeIKHint,HumanBodyBones.RightUpperLeg,HumanBodyBones.RightLowerLeg,HumanBodyBones.RightFoot);
		LeftLeg = new BodyMember(animator,leftFootIKGoal,leftKneeIKHint,HumanBodyBones.LeftUpperLeg,HumanBodyBones.LeftLowerLeg,HumanBodyBones.LeftFoot);
		
		enabledIK = true;
		SaveCurrentPose();
		pelvis = GameObject.Find("pelvis").transform;

		foreach (var rb in GetComponentsInChildren<Rigidbody>())
		{
			Ragdol.Add(rb);
			rb.isKinematic=true;
		}

		

	}
	
	// Update is called once per frame
	void Update () {
		
		if (enabledIK)
		{
		// ToggleControllers(true);
		animator.Update(0);
		// Debug.Log("active");

		GoalControllerBounds(RightHand);
		GoalControllerBounds(LeftHand);
		GoalControllerBounds(RightLeg);
		GoalControllerBounds(LeftLeg);

		HintControllerBounds(RightHand);
		HintControllerBounds(LeftHand);
		HintControllerBounds(RightLeg);
		HintControllerBounds(LeftLeg);
		}
		else
		{	
			// Debug.Log("inactive");
			// ToggleControllers(false);
		}
	}

	void OnAnimatorIK(int layerIndex)
	{
		if (enabledIK)
		{	
			
			
			animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, GoalWeight);
			animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, GoalWeight);
			animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, GoalWeight);
			animator.SetIKPositionWeight(AvatarIKGoal.RightHand, GoalWeight);

			animator.SetIKHintPositionWeight(AvatarIKHint.RightKnee,hintsWeight);
			animator.SetIKHintPositionWeight(AvatarIKHint.LeftKnee,hintsWeight);
			animator.SetIKHintPositionWeight(AvatarIKHint.RightElbow,hintsWeight);
			animator.SetIKHintPositionWeight(AvatarIKHint.LeftElbow,hintsWeight);

			animator.SetIKHintPosition(AvatarIKHint.LeftKnee,LeftLeg.GetHintPos());
			animator.SetIKHintPosition(AvatarIKHint.RightKnee,RightLeg.GetHintPos());
			animator.SetIKHintPosition(AvatarIKHint.LeftElbow,LeftHand.GetHintPos());
			animator.SetIKHintPosition(AvatarIKHint.RightElbow,RightHand.GetHintPos());
			
			animator.SetIKPosition(AvatarIKGoal.LeftFoot, LeftLeg.GetGoalPos());
			animator.SetIKPosition(AvatarIKGoal.RightFoot, RightLeg.GetGoalPos());
			animator.SetIKPosition(AvatarIKGoal.LeftHand, LeftHand.GetGoalPos());
			animator.SetIKPosition(AvatarIKGoal.RightHand, RightHand.GetGoalPos());

		}
		// else
		// {	
		// 	// Debug.Log("inactive");
		// 	// ToggleControllers(false);
		// 	// LeftLeg.SetHintPos(animator.GetIKHintPosition(AvatarIKHint.LeftKnee));
		// 	// RightLeg.SetHintPos(animator.GetIKHintPosition(AvatarIKHint.RightKnee));
		// 	// LeftHand.SetHintPos(animator.GetIKHintPosition(AvatarIKHint.LeftElbow));
		// 	// RightHand.SetHintPos(animator.GetIKHintPosition(AvatarIKHint.RightElbow));

			// LeftLeg.SetGoalPos(animator.GetIKPosition(AvatarIKGoal.LeftFoot));
			// RightLeg.SetGoalPos(animator.GetIKPosition(AvatarIKGoal.RightFoot));
			// LeftHand.SetGoalPos(animator.GetIKPosition(AvatarIKGoal.LeftHand));
			// RightHand.SetGoalPos(animator.GetIKPosition(AvatarIKGoal.RightHand));

		// }

	}

	void GoalControllerBounds(BodyMember member){
		var GoalVector = member.GetGoalPos()-member.BonePos(0);
		if(GoalVector.magnitude>member.length[0]){
			member.SetGoalPos(member.BonePos(2));
		}
	}

	void HintControllerBounds(BodyMember member){
		if(!member.IsHintSelected()){
			member.SetHintPos(member.BonePos(1));
			if(member.HintActive){
				member.HintActive=false;
				member.ToggleHintSelect(false);
			}
			return;
		}
		if(!member.HintActive){
			member.HintActive=true;
			member.ToggleHintSelect(true);
		}
		
		var HintVector = member.GetHintPos()-member.BonePos(0);
		if(HintVector.magnitude!=member.length[1]){
			member.SetHintPos(member.BonePos(1));
		}


	}

	public void ToggleRagdoll(bool active){
		foreach (var rb in Ragdol)
		{
			rb.isKinematic=!active;
		}
	}
	public void ToggleRagdollGravity(bool active){
		// if(Ragdol[0].useGravity== active) return;
		foreach (var rb in Ragdol)
		{
			rb.useGravity=active;
			if (active){
				float fact = Random.Range(1f,2f);
				rb.velocity = new Vector3(0,-0.5f*PushVelocity*fact,PushVelocity*fact);
				rb.AddForce(Vector3.forward*PushForce*fact);
				// rb.AddForce(Vector3.down*10f*PushForce*fact);
				
			}
		}
	}
	public void ToggleControllers(bool active){
		if(fullController.activeSelf==active) return;
		fullController.SetActive(active);

		leftFootIKGoal.GetComponent<ControllerScript>().ToggleActive(active);
		rightFootIKGoal.GetComponent<ControllerScript>().ToggleActive(active);
		leftHandIKGoal.GetComponent<ControllerScript>().ToggleActive(active);
		rightHandIKGoal.GetComponent<ControllerScript>().ToggleActive(active);

		leftKneeIKHint.GetComponent<ControllerScript>().ToggleActive(active);
		rightKneeIKHint.GetComponent<ControllerScript>().ToggleActive(active);
		leftElbowIKHint.GetComponent<ControllerScript>().ToggleActive(active);
		rightElbowIKHint.GetComponent<ControllerScript>().ToggleActive(active);	
		
		ToggleRagdollGravity(active);
		ToggleRagdoll(!active);
	}

	public IEnumerator SetStartPos(){
		// yield return new WaitForEndOfFrame();
		// animator.enabled = false;
		//  yield return new WaitForSeconds(Time.deltaTime*2);
		yield return new WaitForEndOfFrame();
		transform.parent.position = ManStartPos; 
		transform.parent.eulerAngles = Vector3.zero;
		transform.eulerAngles = Vector3.zero;
		transform.localPosition = Vector3.zero;
		// pelvis.localPosition = StartPelvis;
		LeftLeg.SetStartPos();
		RightLeg.SetStartPos();
		LeftHand.SetStartPos();
		RightHand.SetStartPos();
		SaveCurrentPose();
	}
	public void FinSetStartPos(float duration){
		transform.parent.DOMove(ManStartPos,duration);
		transform.parent.DORotate(Vector3.zero,duration);
		LeftLeg.SetStartPos(duration);
		RightLeg.SetStartPos(duration);
		LeftHand.SetStartPos(duration);
		RightHand.SetStartPos(duration);
	}
	public IEnumerator RestartPos(){
		// yield return new WaitForSeconds(Time.deltaTime);
		yield return new WaitForEndOfFrame();

		float duration = 0.5f;
		enabledIK = true;
		//  FinSetStartPos(duration);
		transform.DOLocalRotate( Vector3.zero,duration); 
		transform.DOLocalMove( Vector3.zero,duration); 
		// yield return new WaitForSeconds(1);
		// LeftLeg.SetStartPos(duration);
		// RightLeg.SetStartPos(duration);
		// LeftHand.SetStartPos(duration);
		// RightHand.SetStartPos(duration);
		ApplySavedPose(duration);
	}

	public void GoOnFloor(float duration){
		Vector3 floorpos = new Vector3(0,-3.47f,0);
		// float height = transform.parent.position.z+2;
		
		transform.parent.DOMove(floorpos,duration);
	
	}

	public void SaveCurrentPose(){
		CurrentPose[0] = RightHand.GetGoalPos();
		CurrentPose[1] = RightHand.GetHintPos();
		CurrentPose[2] = LeftHand.GetGoalPos();
		CurrentPose[3] = LeftHand.GetHintPos();
		CurrentPose[4] = RightLeg.GetGoalPos();
		CurrentPose[5] = RightLeg.GetHintPos();
		CurrentPose[6] = LeftLeg.GetGoalPos();
		CurrentPose[7] = LeftLeg.GetHintPos();
		CurrentPose[8]=transform.parent.position;
		CurrentPose[9]=transform.parent.eulerAngles;
	}

	public void ApplySavedPose(){
		RightHand.SetGoalPos(CurrentPose[0]);
		RightHand.SetHintPos(CurrentPose[1]);
		LeftHand.SetGoalPos(CurrentPose[2]);
		LeftHand.SetHintPos(CurrentPose[3]);
		RightLeg.SetGoalPos(CurrentPose[4]);
		RightLeg.SetHintPos(CurrentPose[5]);
		LeftLeg.SetGoalPos(CurrentPose[6]);
		LeftLeg.SetHintPos(CurrentPose[7]);
		transform.parent.position = CurrentPose[8];
		transform.parent.eulerAngles = CurrentPose[9];
	}
	public void ApplySavedPose(float duration){
		RightHand.SetGoalPos(CurrentPose[0],duration);
		RightHand.SetHintPos(CurrentPose[1],duration);
		LeftHand.SetGoalPos(CurrentPose[2],duration);
		LeftHand.SetHintPos(CurrentPose[3],duration);
		RightLeg.SetGoalPos(CurrentPose[4],duration);
		RightLeg.SetHintPos(CurrentPose[5],duration);
		LeftLeg.SetGoalPos(CurrentPose[6],duration);
		LeftLeg.SetHintPos(CurrentPose[7],duration);
		transform.parent.DOMove(CurrentPose[8],duration);
		transform.parent.DORotate(CurrentPose[9],duration);
	}

}
