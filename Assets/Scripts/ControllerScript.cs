﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ControllerScript : MonoBehaviour
{
    public BodyMember Member;
    public int Type;//0 - goal; 1 - hint
    public float duration = 0.2f;
    public Color ActiveColor = new Color(1,1,1,0.65f);
    public Color InactiveColor = new Color(1,1,1,0.4f);
    Vector3 InactiveScale;
    Vector3 ActiveScale;

    SticmanIK SIK;

    void Start(){
        SIK = FindObjectOfType<SticmanIK>();
        InactiveScale = transform.localScale;
        ActiveScale = 1.5f*InactiveScale;
    }
    
    void OnMouseDown(){
        // Vector3 sc = transform.localScale*1.5f;
        transform.DOScale(ActiveScale,duration);
        GetComponent<Renderer>().material.DOColor(ActiveColor,duration);
    }
    void OnMouseUp(){
        // Vector3 sc = transform.localScale/1.5f;
        transform.DOScale(InactiveScale,duration);
        GetComponent<Renderer>().material.DOColor(InactiveColor,duration);
        SIK.SaveCurrentPose();
    }
    // void OnCollisionEnter(Collision collision){
    //     Debug.Log(collision.gameObject.name);
    //     // SIK.ToggleRagdoll(true);
    // }

    //     void OnTriggerEnter(Collider other){
    //     // Debug.Log("qqqq");
    //     // SIK.ToggleRagdollGravity(true);
    // }


    public void ToggleActive(bool Active){
        //стремный костыль. если отключить полностью gameObject контроллера через setActive(false), а потом включить,
        //lean drag translate начинает работать дергано. Поэтому просто скрываю визуально.
        GetComponent<Renderer>().enabled = Active;
        GetComponent<Collider>().enabled = Active;
    }
}
