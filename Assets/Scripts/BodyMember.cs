﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;
using DG.Tweening;
public class BodyMember 
{
    Animator anim;
    LeanSelect LS;
    public GameObject Hint;
    public GameObject Goal;
    public List<HumanBodyBones> Bones= new List<HumanBodyBones>();
    public float[] length = new float[2];
    public bool HintActive = false;
    public Vector3 StartGoalPos;
    public Vector3 StartHintPos;

    public BodyMember(Animator animator,GameObject goal,GameObject hint,HumanBodyBones bone1,HumanBodyBones bone2,HumanBodyBones bone3){
        anim = animator;
        Hint = hint;
        Goal = goal;
            Goal.GetComponent<ControllerScript>().Member = this;
            Hint.GetComponent<ControllerScript>().Member = this;
            Goal.GetComponent<ControllerScript>().Type = 0;
            Hint.GetComponent<ControllerScript>().Type = 1;
        Bones.Add(bone1);
        Bones.Add(bone2);
        Bones.Add(bone3);
        StartGoalPos = GetGoalPos();
        StartHintPos = GetHintPos();
        length[0] =(anim.GetBoneTransform(bone1).position - anim.GetBoneTransform(bone3).position).magnitude;
        length[1] =(anim.GetBoneTransform(bone1).position - anim.GetBoneTransform(bone2).position).magnitude;
        LS = Object.FindObjectOfType<LeanSelect>();

    }
    public Transform BoneTransform(int i){
        return anim.GetBoneTransform(Bones[i]);
    }
    public Vector3 BonePos(int i){
        return anim.GetBoneTransform(Bones[i]).position;
    }
    public Vector3 GetGoalPos(){
        return Goal.transform.position;
    }
    public void SetGoalPos(Vector3 pos){
        if(double.IsNaN(pos.x)||double.IsNaN(pos.y)||double.IsNaN(pos.z)) {
            Debug.Log("NAN goal pos");
            return;
        }    
        // Goal.transform.DOMove(pos,0.01f);
        Goal.transform.position = pos;
    }
    public void SetGoalPos(Vector3 pos,float duration){
        if(double.IsNaN(pos.x)||double.IsNaN(pos.y)||double.IsNaN(pos.z)) {
            Debug.Log("NAN goal pos");
            return;
        }    
        Goal.transform.DOMove(pos,duration);
    }
    public Vector3 GetHintPos(){
        return Hint.transform.position;
    }
    public void SetHintPos(Vector3 pos){
        if(double.IsNaN(pos.x)||double.IsNaN(pos.y)||double.IsNaN(pos.z)) {
            Debug.Log("NAN hint pos");
            return;
        }
        // Hint.transform.DOMove(pos,0.01f);
        Hint.transform.position = pos;
    }
    public void SetHintPos(Vector3 pos,float duration){
        if(double.IsNaN(pos.x)||double.IsNaN(pos.y)||double.IsNaN(pos.z)) {
            Debug.Log("NAN hint pos");
            return;
        }    
        Hint.transform.DOMove(pos,duration);
    }
    public bool IsHintSelected(){
        return Hint.GetComponent<LeanSelectable>().IsSelected;
    }

	public void ToggleHintSelect(bool select){
			var GoalSelect=Goal.GetComponent<LeanSelectable>();
			// var GoalSelectRend = Goal.GetComponent<LeanSelectableRendererColor>();
			// GoalSelectRend.enabled = !select;
		if(select){
			LS.MaxSelectables = 2;
			GoalSelect.Select();
		}else{
			LS.MaxSelectables = 1;
            GoalSelect.Deselect();
		}
	}

    public void SetStartPos(){
        SetGoalPos(StartGoalPos);
        SetHintPos(StartHintPos);
    }

    public void SetStartPos(float duration){
        SetGoalPos(StartGoalPos,duration);
        SetHintPos(StartHintPos,duration);
    }

}
