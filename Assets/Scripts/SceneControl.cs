﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SceneControl : MonoBehaviour
{   
    public GameObject Ball;
    public GameObject PhysBall;
    public float BallSize;

    public Transform Pool;
    public List<Animator> People = new List<Animator>();
    public List<Color> BallColors = new List<Color>();
     List<Vector3> PeopleStartPos = new List<Vector3>();

    void Start(){
        foreach (var human in People){
            PeopleStartPos.Add(human.transform.position);
        }
        SpawnBalls(BallSize);
    }

    public void Clap(){
        foreach (var anim in People)
        {
            anim.SetTrigger("Clap");
        }
        Invoke("SitDown",3.5f);
    }

    public void SitDown(){
        for (int i = 0; i < People.Count; i++)
        {
           People[i].transform.DOMove(PeopleStartPos[i],0.5f); 
        }

    }

    void SpawnBalls(float size){
        Vector3 zeropos = new Vector3(-4.4f+0.5f*size,-4,0.76f+0.5f*size);
        Vector2 PoolSize = new Vector2(11,10);
        int m = (int) Mathf.Round(PoolSize.x/size);
        int n = (int) Mathf.Round(PoolSize.y/size);

        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                Vector3 pos = new Vector3(zeropos.x+i*size,zeropos.y,zeropos.z+j*size);
                var ball = Instantiate(Ball,pos,Quaternion.identity,Pool);
                ball.GetComponent<Renderer>().material.color = RndColor();
            }
        }
    }

    public void Plop(Vector3 pos){
        int n = Random.Range(3,8);
        float lifetime = 5;
        // Debug.Log("Plop "+n);
        GameObject[] balls = new GameObject[n];

        for (int i = 0; i < n; i++)
        {
            var ball = Instantiate(PhysBall,pos+Vector3.up*2,Quaternion.identity);
            ball.GetComponent<Rigidbody>().velocity = Vector3.up*6f;
            ball.GetComponent<Rigidbody>().AddExplosionForce(2,pos-1*Vector3.down,2);
            // ball.GetComponent<Rigidbody>().AddForce(Vector3.up*100);
            
            ball.GetComponent<Renderer>().material.color = RndColor();
            Destroy(ball,lifetime);
        }

    }

    Color RndColor(){
        int i = Random.Range(0,BallColors.Count);
        return BallColors[i];
    }
}
