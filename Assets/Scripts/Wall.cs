﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class Wall : MonoBehaviour
{

    public SticmanIK SIK;

    void OnCollisionEnter(Collision collision){
        Test(collision);
    }

    private void OnCollisionStay(Collision collision)
    {
        Test(collision);
    }
    private void Test(Collision collision)
    {
        if (SIK == null)
        {
            SIK = GetComponentInParent<Wall>().SIK;
        }
        if (collision.gameObject.tag == "Ragdoll")
        {
            var collisionPoint = collision.GetContact(0).point;
            if (!IsPointInWall(collisionPoint))
            {
                // Debug.Log(" collision " + collision.gameObject.name + "   " + collisionPoint);
                return;
            }

            // Debug.Log(collision.gameObject.name + "   " + collisionPoint);

            // Debug.DrawLine(Vector3.zero, collision.GetContact(0).point, Color.red, 10);
            SIK.enabledIK = false;
            SIK.animator.enabled = false;
            SIK.ToggleRagdollGravity(true);
            SticmanIK.Fail = true;
            // collision.rigidbody.AddForce(Vector3.forward*5);
            // collision.rigidbody.AddForce(Vector3.forward*SIK.PushForce);
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, $"Fail level {GameManager.lvl}");
        }
        else
        {
            // Debug.Log("other" + collision.gameObject.name);
        }
    }

    private static bool IsPointInWall(Vector3 point)
    {
        return Physics.Raycast(new Ray(point - new Vector3(0, 0, 1), new Vector3(0, 0, 1)), 2, 1 << 8);
    }
}
